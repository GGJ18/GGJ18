﻿// created by Alex

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRig : MonoBehaviour {

    [SerializeField] Transform target;

	float lerpFactor = 0.05f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		transform.position = Vector3.Lerp (transform.position, target.position, lerpFactor);

		transform.Rotate(new Vector3(0,Input.GetAxis("Mouse X"),0));
		
	}
}

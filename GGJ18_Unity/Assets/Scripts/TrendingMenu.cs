﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using UnityCommon;
using UnityEngine;
using TMPro;

public class TrendingMenu : MonoBehaviour {

    public QuestionnaireMenu questionnaireMenu;
    public SongManager songManager;
    public Canvas canvas;
    public RectTransform parent;
    public GameObject songTextPrefab;
    public float songTextHeight = 50f;
    public List<string> questionTemplate;
    public List<string> positiveReplyTemplate;
    public List<string> neutralReplyTemplate;
    public List<string> negativeReplyTemplate;

    public TextMeshProUGUI subtitleText;
    public float timeTillReply = 3f;
    public float timeTillReplyDisappears = 5f;

    [Header("Debug Only")]
    public List<TextMeshProUGUI> compareOptions;

    private Camera pCamera;

    private List<int> comparableList;
    private int selectedSong = -1;
    private int questionIdx;
    private int replyIdx;

    private StateMachine pStateMachine = new StateMachine();

    public int localToIdx(float y) {
        return (int) (-y / songTextHeight);
    }

    public float idxToLocal(int idx) {
        return -idx * songTextHeight;
    }

    public TextMeshProUGUI InstantiateSongText(int listIdx, int songIdx, SongManager.SongInfo song, Transform parent) {
        var textGo = Instantiate(songTextPrefab, parent);
		var text = textGo.GetComponentInChildren<TextMeshProUGUI>();
		text.color = songManager.songs[songIdx].color;
		var rectT = text.transform.parent.GetComponent<RectTransform>();
        text.text = string.Format("{0} by {1}", song.name, song.band);
        rectT.anchoredPosition = new Vector2(0, idxToLocal(listIdx));
        return text;
    }

    private void SetSubtitleTextQuestion() {
        var blankStr = "__________";
        var question = questionTemplate[questionIdx].Replace("\\n", "\n"); // "Hey listener, is \"{0}\" trending?"

        if( selectedSong == -1 )
            subtitleText.text = string.Format(question, blankStr, blankStr);
        else
            subtitleText.text = string.Format(question,
                string.Format("<color=#{0}>{1}</color>", CompareMenu.RGBToHex(songManager.songs[comparableList[selectedSong]].color), songManager.songs[comparableList[selectedSong]].name));
    }

    private void SetSubtitleTextReply() {
        var velocity = songManager.songs[comparableList[selectedSong]].velocity;
        var replyTemplate = velocity > 0 ? positiveReplyTemplate : (velocity == 0) ? neutralReplyTemplate : negativeReplyTemplate;

        replyIdx = UnityEngine.Random.Range(0, replyTemplate.Count());
        var reply = replyTemplate[replyIdx].Replace("\\n", "\n");
        //var reply = "yes \"{0}\" is trending.";
        subtitleText.text = string.Format(reply,
            string.Format("<color=#{0}>{1}</color>", CompareMenu.RGBToHex(songManager.songs[selectedSong].color), songManager.songs[selectedSong].name));
    }

    private void ClearSubtitleText() {
        subtitleText.text = "";
    }

    public void Init() {
        questionIdx = UnityEngine.Random.Range(0, questionTemplate.Count());
        selectedSong = -1;
        SetSubtitleTextQuestion();
        CreateItems();
    }

    private void CreateItems() {
        comparableList = songManager.songs.Select((song, songIdx) => songIdx).Where((songIdx) => songIdx != selectedSong).ToList();
        comparableList.ForIndexEach((comparableIdx, songIdx) => {
            var song = songManager.songs[songIdx];
            var text = InstantiateSongText(comparableIdx, songIdx, song, parent.transform);
            compareOptions.Add(text);
        });
    }

    private void DestroyItems() {
        while( compareOptions.Any() ) {
			Destroy(compareOptions[0].transform.parent.gameObject);
            compareOptions.RemoveAt(0);
        }
    }

    private void Awake() {
        // Get a reference to the camera if Canvas Render Mode is not ScreenSpace Overlay.
        if( canvas.renderMode == RenderMode.ScreenSpaceOverlay )
            pCamera = null;
        else
            pCamera = canvas.worldCamera;
    }

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        if( Input.GetMouseButtonUp(0) ) {
            var compareIdx = compareOptions.FindIndex((candidate) => TMP_TextUtilities.IsIntersectingRectTransform(candidate.rectTransform, Input.mousePosition, pCamera));

            if( compareIdx >= 0 ) {
                if( selectedSong == -1 ) {
                    selectedSong = compareIdx;
                    DestroyItems();
                    SetSubtitleTextQuestion();
                    pStateMachine.AddDelay(timeTillReply, () => {
                        SetSubtitleTextReply();
                        pStateMachine.AddDelay(timeTillReplyDisappears, questionnaireMenu.BackToQuestionnaireMenu);
                    });
                }
            }
        }

        pStateMachine.Update();
    }
}

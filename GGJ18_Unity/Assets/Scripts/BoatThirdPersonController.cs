﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using UnityCommon;
using UnityEngine;

public class BoatThirdPersonController : MonoBehaviour {

    public GameManager gameManager;
    public Transform boat;
    public Transform lookTransform;
    public Camera controlCamera;
    public float positionSmoothRate = 1f;
    public float rotationSmoothRate = 1f;
    public float fullRotationScreenDistance = 100;
    public GameObject exitPrompt;

    public AudioSource boatEngine;
    public float maxEngineVolume;
    public float minEngineVolume;

    private float? originalScreenX = 0;
    private Quaternion virtualRotation;
    private bool active = false;

    private Boat pBoatComp;

    private void Awake() {
        pBoatComp = boat.GetComponent<Boat>();

        virtualRotation = lookTransform.rotation;
    }

    public void Activate() {
        active = true;
        controlCamera.gameObject.SetActive(true);
        exitPrompt.SetActive(true);
    }

    public void Deactivate() {
        active = false;
        controlCamera.gameObject.SetActive(false);
        exitPrompt.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        if(active) {
            var h = Input.GetAxis("Horizontal");
            var v = Input.GetAxis("Vertical");
            pBoatComp.HandleMovement(h, v);
        }
        else {
            pBoatComp.HandleMovement(0, 0);
        }

        if( active && !gameManager.isGameOver && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Tab)) ) {
            gameManager.SwitchToFirstPerson();
            return;
        }
        
        lookTransform.transform.position = Vector3.Lerp(lookTransform.transform.position, boat.transform.position + boat.transform.forward * 10f, positionSmoothRate * Time.deltaTime);
        lookTransform.rotation = virtualRotation = Quaternion.Euler(Quaternion.Slerp(virtualRotation, boat.rotation, rotationSmoothRate * Time.deltaTime).eulerAngles.WithX(0).WithZ(0));

        if( Input.GetMouseButton(0) || Input.GetMouseButton(1) ) {
            if( originalScreenX.HasValue ) {
                var playerRotateY = (Input.mousePosition.x - originalScreenX.Value) / fullRotationScreenDistance * 360f;
                lookTransform.Rotate(0, playerRotateY, 0);
            }
            else {
                originalScreenX = Input.mousePosition.x;
            }
        }
        else {
            originalScreenX = null;
        }

        boatEngine.volume = (pBoatComp.velocity - pBoatComp.minSpeed) / (pBoatComp.maxSpeed - pBoatComp.minSpeed) * (maxEngineVolume - minEngineVolume) + minEngineVolume;
    }
}

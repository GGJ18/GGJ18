﻿//
// Modified version of UnityStandardAssets.Characters.FirstPerson.FirstPersonController
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common;
using UnityCommon;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using URand = UnityEngine.Random;
using UnityStandardAssets.Characters.FirstPerson;

public class FPController : MonoBehaviour {
    [SerializeField] private float m_WalkSpeed;
    [SerializeField] private float m_RunSpeed;
    [SerializeField] private float m_LadderUpSpeed;
    [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
    [SerializeField] private float m_JumpSpeed;
    [SerializeField] private float m_StickToGroundForce;
    [SerializeField] private float m_GravityMultiplier;
    [SerializeField] private bool m_UseFovKick;
    [SerializeField] private FOVKick m_FovKick = new FOVKick();
    [SerializeField] private bool m_UseHeadBob;
    [SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
    [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
    [SerializeField] private float m_StepInterval;

    private Camera m_Camera;
    private float m_YRotation;
    private Vector3 m_MoveDir = Vector3.zero;
    private CollisionFlags m_CollisionFlags;
    private bool m_PreviouslyGrounded;
    private Vector3 m_OriginalCameraPosition;
	private CapsuleCollider m_capsuleCollider;

    private bool m_IsInLadderZone;
    private bool m_Jumping;

    public Vector3 Velocity { get { return m_MoveDir; } }

    public bool isControlsEnabled = true;
	public bool isMouseControlEnabled = true;

    private const float cmdSyncInterval = 0.1f;
    private float cmdSyncTime = 0f;
	    
    float verticalMovement = 0.0f;
	
	

    // controls
    private bool m_Jump;
    private Vector2 m_Input;
    [SerializeField]
    private bool m_IsWalking;
    [SerializeField] private MouseLook m_MouseLook;
    [SerializeField] private float m_Speed;

    public static Ladder[] ladders;
	
    public void SetCursorLock(bool value) {
        m_MouseLook.SetCursorLock(value);
    }
	
	Vector3[] positionBuffer = new Vector3[2];
	
	private void Awake()
	{
		m_capsuleCollider = GetComponent<CapsuleCollider>();
        m_Camera = GetComponentInChildren<Camera>();
		m_MouseLook.Init(transform, m_Camera.transform);
        m_MouseLook.clampVerticalRotation = true;
	}

    // Use this for initialization
    private void Start() {
		
        m_OriginalCameraPosition = m_Camera.transform.localPosition;
        m_FovKick.Setup(m_Camera);
        m_HeadBob.Setup(m_Camera, m_StepInterval);
        m_Jumping = false;
        

        ladders = FindObjectsOfType<Ladder>();
    }

	void LateUpdate()
	{
	}


    // Update is called once per frame
    private void Update() {
		if(isMouseControlEnabled)
			RotateView();
		
        // the jump state needs to read here to make sure it is not missed
        if( !m_Jump ) {
            m_Jump = CrossPlatformInputManager.GetButton("Jump");
        }
		
	}

    private void SyncInput() {
        CmdSyncInput(m_Input, m_MoveDir.y, m_IsWalking);
    }
    
    private void CmdSyncInput(Vector2 input, float vertical_mov, bool walking) {
        m_Input = input;
        verticalMovement = vertical_mov;
        m_IsWalking = walking;
    }

    // map input movement from square to unit sphere.
    private void FixInputMovement() {
        // normalize input if it exceeds 1 in combined length:
        //if (m_Input.sqrMagnitude > 1)
        //{
        //    m_Input.Normalize();
        //}

        if( m_Input != Vector2.zero ) {
            Vector2 ex;
            if( Math.Abs(m_Input.x) > Math.Abs(m_Input.y) )
                ex = new Vector2(1f, m_Input.y / m_Input.x);
            else
                ex = new Vector2(m_Input.x / m_Input.y, 1f);

            var angle = Vector2.SignedAngle(Vector2.right, ex) * Mathf.Deg2Rad;

            var onCircle = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

            var original = m_Input;
            m_Input = new Vector2(m_Input.x != 0f ? (m_Input.x * onCircle.x / ex.x) : 0f, m_Input.y != 0f ? (m_Input.y * onCircle.y / ex.y) : 0f);
        }
    }

    private void OnTriggerStay(Collider other) {
        m_IsInLadderZone = m_IsInLadderZone || (ladders.Any((ladder) => ladder.gameObject == other.gameObject));
    }

    private void FixedUpdate() {
        // set the desired speed to be walking or running
        m_Speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
        if( isControlsEnabled ) {
            m_Input = Vector2.zero;
            GetInput();
        }

        // always move along the camera forward as it is the direction that it being aimed at
        Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

        // get a normal for the surface that is being touched to move along it
        RaycastHit hitInfo;
        Physics.SphereCast(transform.position, m_capsuleCollider.radius, Vector3.down, out hitInfo,
                           m_capsuleCollider.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
        desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal);

        m_MoveDir.x = desiredMove.x * m_Speed;
        m_MoveDir.z = desiredMove.z * m_Speed;

        bool isClimbing = m_IsInLadderZone && m_Input.y > 0;

        if( isClimbing )
            m_MoveDir.y = m_LadderUpSpeed;

        if( !isControlsEnabled ) {
            m_MoveDir.y = verticalMovement;

            if( !isClimbing )
                m_MoveDir.y += (Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime).y;
        }
		
		/*
        if( !isClimbing ) {
            if( isGrounded ) {
                if( m_Jump ) {
                    m_MoveDir.y = m_JumpSpeed;
                    m_Jumping = true;
                }
                else {
                    m_MoveDir.y = -m_StickToGroundForce;
                }
            }
            else {
                m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
            }
        }
		
		*/
		
		//print(m_GravityMultiplier);
		
		if( !isClimbing )
		{
			m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
		}
		

        m_Jump = false;
		
		transform.position += m_MoveDir * Time.fixedDeltaTime;

        UpdateCameraPosition(m_Speed);

        m_MouseLook.UpdateCursorLock();

        if( isControlsEnabled ) {
            cmdSyncTime += Time.deltaTime;
            if( cmdSyncTime >= cmdSyncInterval ) {
                cmdSyncTime = 0f;
                SyncInput();
            }
        }

        m_IsInLadderZone = false;
    }

    private void UpdateCameraPosition(float speed) {
        Vector3 newCameraPosition;
        if( !m_UseHeadBob ) {
            return;
        }
    }


    private void GetInput() {
        // Read input
        float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        float vertical = CrossPlatformInputManager.GetAxis("Vertical");

        bool waswalking = m_IsWalking;

#if !MOBILE_INPUT
        // On standalone builds, walk/run speed is modified by a key press.
        // keep track of whether or not the character is walking or running
        m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
        m_Input = new Vector2(horizontal, vertical);

        FixInputMovement();
    }

    public void SetRotation(Quaternion rotation) {
        //transform.rotation = rotation;
        m_MouseLook.SetRotation(rotation);
        m_Camera.transform.rotation = rotation;
    }


    private void RotateView() {
        if( isControlsEnabled )
            m_MouseLook.LookRotation(transform, m_Camera.transform);
    }
	
	private void OnCollisionStay(Collision col)
	{
		//print(col.contacts[0].separation);
		transform.position -= col.contacts[0].normal * (float)(0.01 + col.contacts[0].separation);
		
		if(col.contacts[0].normal.y > 0.8)
		{
			m_MoveDir.y = 0;
		}
		
	}
	
}

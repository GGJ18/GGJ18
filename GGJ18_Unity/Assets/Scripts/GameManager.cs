﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Common;
using UnityCommon;
using TMPro;
using UnityEngine.Playables;

public class GameManager : MonoBehaviour {

    public bool isLeftSideFirstPerson;
    public Transform leftRespawn;
    public Transform rightRespawn;
    public FPController firstPersonController;
    public BoatThirdPersonController thirdPersonController;
    public PlayableDirector promptDirector;
    public GameObject deadCanvas;
    public QuestionnaireMenu questionnaireMenu;
    public PlaylistView playlistView;

    public bool isGameOver { get { return deadCanvas.activeSelf; } }

    [Header("Debug Only")]
    public bool hasPromptBeenFulfilled = false;

    // Use this for initialization
    void Start() {

    }

    public void SwitchToFirstPerson() {
        thirdPersonController.Deactivate();
        firstPersonController.gameObject.SetActive(true);
        firstPersonController.isMouseControlEnabled = true;
        firstPersonController.SetCursorLock(true);
		Cursor.visible = false;
        var spawnTransform = (isLeftSideFirstPerson ? leftRespawn : rightRespawn);
        firstPersonController.gameObject.transform.position = spawnTransform.position;
        firstPersonController.SetRotation(Quaternion.LookRotation(spawnTransform.rotation * Vector3.forward, Vector3.up));
    }

    public void SwitchToThirdPerson(bool isLeft) {
        isLeftSideFirstPerson = isLeft;
        thirdPersonController.Activate();
        firstPersonController.gameObject.SetActive(false);
        firstPersonController.isMouseControlEnabled = false;
        firstPersonController.SetCursorLock(false);
        Cursor.visible = false;
    }

    public void DeactivatePrompt() {
        promptDirector.gameObject.SetActive(false);
    }

    public void ActivatePrompt() {
        if( !hasPromptBeenFulfilled && !promptDirector.gameObject.activeSelf ) {
            promptDirector.gameObject.SetActive(true);
            promptDirector.Play();
        }
    }

    // Update is called once per frame
    void Update() {
        if( isGameOver && Input.GetKeyDown(KeyCode.Escape) )
            Application.Quit();
    }
    
    private void GameOver() {
        Time.timeScale = 0;
        SwitchToThirdPerson(true);
        deadCanvas.SetActive(true);
        questionnaireMenu.CloseQuestionnaireMenu();
        playlistView.Close();
        DeactivatePrompt();
        firstPersonController.isMouseControlEnabled = false;
        firstPersonController.SetCursorLock(false);
        thirdPersonController.exitPrompt.SetActive(false);
    }

    public void LoseGameFromCollisionWithBaddie(Transform baddie) {
        GameOver();
        Camera.main.transform.parent.position = baddie.position;
    }

    public void LoseGameFromMinimumListeners() {
        GameOver();
        deadCanvas.GetComponentInChildren<Text>().text = "GAME OVER, No listeners :(";
    }
}

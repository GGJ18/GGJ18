﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using UnityCommon;
using UnityEngine;
using URand = UnityEngine.Random;

public class IcebergManager : MonoBehaviour {

    public GameObject playerShip;
    public GameObject parent;
    public GameObject prefab;
    public int maxSimultaneous = 3;
    public float chancePerCheck = 0.1f;
    public float timePerCheck = 5f;
    public float spawnRadius = 100f;
    public float despawnRadius = 100f;

    private StateMachine pStateMachine = new StateMachine();

    [Header("Debug Only")]
    public List<GameObject> items;

    public static Vector2 Rotate(Vector2 v, float degrees) {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

    private void CheckSpawn() {
        if( items.Count() < maxSimultaneous && URand.value < chancePerCheck ) {
            Vector2 randDirection;
            do {
                randDirection = URand.insideUnitCircle.normalized;
            } while( Vector3.Angle(playerShip.transform.forward, randDirection.PromoteXZ(0)) > 90f );
            
            var go = Instantiate(prefab,
                playerShip.transform.position + (spawnRadius * randDirection).PromoteXZ(0),
                Quaternion.Euler(0, URand.Range(0, 360f), 0),
                parent.transform);
            items.Add(go);
        }
    }

    // Use this for initialization
    void Start() {
        QueueSpawn();
    }

    void QueueSpawn() {
        pStateMachine.AddDelay(timePerCheck, () => { CheckSpawn(); QueueSpawn(); });
    }

    // Update is called once per frame
    void Update() {
        pStateMachine.Update();

        for( int i = 0; i < items.Count(); ) {
            if( (items[i].transform.position - playerShip.transform.position).magnitude > despawnRadius ) {
                Destroy(items[i].gameObject);
                items.RemoveAt(i);
            }
            else {
                ++i;
            }
        }
    }
}

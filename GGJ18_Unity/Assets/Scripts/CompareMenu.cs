﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using UnityCommon;
using UnityEngine;
using TMPro;

public class CompareMenu : MonoBehaviour {

    public QuestionnaireMenu questionnaireMenu;
    public SongManager songManager;
    public Canvas canvas;
    public RectTransform parent;
    public GameObject songTextPrefab;
    public float songTextHeight = 120f;
    public List<string> questionTemplate;
    public List<string> replyTemplate;

    public TextMeshProUGUI subtitleText;
    public float timeTillReply = 3f;
    public float timeTillReplyDisappears = 5f;

    [Header("Debug Only")]
    public List<TextMeshProUGUI> compareOptions;

    private Camera pCamera;

    private List<int> comparableList;
    private int selectedFirstSong = -1;
    private int selectedSecondSong = -1;
    private int questionIdx;
    private int replyIdx;

    private StateMachine pStateMachine = new StateMachine();

    public int localToIdx(float y) {
        return (int) (-y / songTextHeight);
    }

    public float idxToLocal(int idx) {
        return -idx * songTextHeight;
    }

    public TextMeshProUGUI InstantiateSongText(int listIdx, int songIdx, SongManager.SongInfo song, Transform parent) {
        var textGo = Instantiate(songTextPrefab, parent);
		var text = textGo.GetComponentInChildren<TextMeshProUGUI>();
		text.color = song.color;
		var rectT = text.transform.parent.GetComponent<RectTransform>();
        text.text = string.Format("{0} by {1}", song.name, song.band);
        rectT.anchoredPosition = new Vector2(0, idxToLocal(listIdx));
        return text;
    }

    public static string GetHex(int dec) {
        var alpha = "0123456789ABCDEF";
        return alpha[dec].ToString();
    }

    public static string RGBToHex(Color color) {
        var red = (int) (color.r * 255);
        var green = (int) (color.g * 255);
        var blue = (int) (color.b * 255);

        var a = GetHex((int) Mathf.Floor(red / 16));
        var b = GetHex((int) Mathf.Round(red % 16));
        var c = GetHex((int) Mathf.Floor(green / 16));
        var d = GetHex((int) Mathf.Round(green % 16));
        var e = GetHex((int) Mathf.Floor(blue / 16));
        var f = GetHex((int) Mathf.Round(blue % 16));

        var z = a + b + c + d + e + f;

        return z;
    }

    private void SetSubtitleTextQuestion() {
        var blankStr = "__________";
        var question = questionTemplate[questionIdx].Replace("\\n", "\n"); // "Hey listener, is \"{0}\" more popular than \"{1}\"?"

        if( selectedFirstSong == -1 )
            subtitleText.text = string.Format(question, blankStr, blankStr);
        else if(selectedSecondSong == -1)
            subtitleText.text = string.Format(question, string.Format("<color=#{0}>{1}</color>", RGBToHex(songManager.songs[selectedFirstSong].color), songManager.songs[selectedFirstSong].name), blankStr);
        else
            subtitleText.text = string.Format(question,
                string.Format("<color=#{0}>{1}</color>", RGBToHex(songManager.songs[selectedFirstSong].color), songManager.songs[selectedFirstSong].name),
                string.Format("<color=#{0}>{1}</color>", RGBToHex(songManager.songs[selectedSecondSong].color), songManager.songs[selectedSecondSong].name));
    }

    private void SetSubtitleTextReply() {
        var reply = replyTemplate[replyIdx].Replace("\\n", "\n");
        //var reply = "\"{0}\" is more popular.";
        int popularIdx, unpopularIdx;
        if( songManager.songs[selectedFirstSong].popularity >= songManager.songs[selectedSecondSong].popularity ) {
            popularIdx = selectedFirstSong;
            unpopularIdx = selectedSecondSong;
        }
        else {
            popularIdx = selectedSecondSong;
            unpopularIdx = selectedFirstSong;
        }
        subtitleText.text = string.Format(reply,
                string.Format("<color=#{0}>{1}</color>", RGBToHex(songManager.songs[popularIdx].color), songManager.songs[popularIdx].name),
                string.Format("<color=#{0}>{1}</color>", RGBToHex(songManager.songs[unpopularIdx].color), songManager.songs[unpopularIdx].name));
    }

    private void ClearSubtitleText() {
        subtitleText.text = "";
    }

    public void Init() {
        questionIdx = UnityEngine.Random.Range(0, questionTemplate.Count());
        replyIdx = UnityEngine.Random.Range(0, replyTemplate.Count());
        selectedFirstSong = -1;
        selectedSecondSong = -1;
        SetSubtitleTextQuestion();
        CreateItems();
    }

    private void CreateItems() {
        comparableList = songManager.songs.Select((song, songIdx) => songIdx).Where((songIdx) => songIdx != selectedFirstSong).ToList();
        comparableList.ForIndexEach((comparableIdx, songIdx) => {
            var song = songManager.songs[songIdx];
            var text = InstantiateSongText(comparableIdx, songIdx, song, parent.transform);
            compareOptions.Add(text);
        });
    }

    private void DestroyItems() {
        while( compareOptions.Any() ) {
			Destroy(compareOptions[0].transform.parent.gameObject);
            compareOptions.RemoveAt(0);
        }
    }

    private void Awake() {
        // Get a reference to the camera if Canvas Render Mode is not ScreenSpace Overlay.
        if( canvas.renderMode == RenderMode.ScreenSpaceOverlay )
            pCamera = null;
        else
            pCamera = canvas.worldCamera;
    }

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        if( Input.GetMouseButtonUp(0) ) {
            var compareIdx = compareOptions.FindIndex((candidate) => TMP_TextUtilities.IsIntersectingRectTransform(candidate.rectTransform, Input.mousePosition, pCamera));

            if( compareIdx >= 0 ) {
                if( selectedFirstSong == -1 ) {
                    selectedFirstSong = comparableList[compareIdx];
                    DestroyItems();
                    CreateItems();
                    SetSubtitleTextQuestion();
                }
                else {
                    selectedSecondSong = comparableList[compareIdx];
                    DestroyItems();
                    SetSubtitleTextQuestion();
                    pStateMachine.AddDelay(timeTillReply, () => {
                        SetSubtitleTextReply();
                        pStateMachine.AddDelay(timeTillReplyDisappears, questionnaireMenu.BackToQuestionnaireMenu);
                    });
                }
            }
        }

        pStateMachine.Update();
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Common;
using UnityCommon;
using TMPro;

public class QuestionnaireMenu : MonoBehaviour {

    public GameManager gameManager;
    public GameObject optionsMenu;
    public CompareMenu comparisonMenu;
    public TrendingMenu trendingMenu;

    public void OpenTrending() {
        optionsMenu.gameObject.SetActive(false);
        trendingMenu.gameObject.SetActive(true);
        trendingMenu.Init();
    }

    public void OpenComparison() {
        optionsMenu.gameObject.SetActive(false);
        comparisonMenu.gameObject.SetActive(true);
        comparisonMenu.Init();
    }

    public void BackToQuestionnaireMenu() {
        trendingMenu.gameObject.SetActive(false);
        comparisonMenu.gameObject.SetActive(false);
        optionsMenu.gameObject.SetActive(true);
    }

    public void OpenQuestionnaireMenu() {
        optionsMenu.gameObject.SetActive(true);
        gameManager.firstPersonController.isMouseControlEnabled = false;
        gameManager.firstPersonController.SetCursorLock(false);
        gameManager.DeactivatePrompt();
    }

    public void CloseQuestionnaireMenu() {
        optionsMenu.gameObject.SetActive(false);
        comparisonMenu.gameObject.SetActive(false);
        trendingMenu.gameObject.SetActive(false);
        gameManager.firstPersonController.isMouseControlEnabled = true;
        gameManager.firstPersonController.SetCursorLock(true);
        gameManager.ActivatePrompt();
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if( Input.GetKeyDown(KeyCode.Escape) ) {
            if( optionsMenu.activeInHierarchy )
                CloseQuestionnaireMenu();
            else if( trendingMenu.gameObject.activeInHierarchy )
                BackToQuestionnaireMenu();
            else if( comparisonMenu.gameObject.activeInHierarchy )
                BackToQuestionnaireMenu();
        }
    }
}

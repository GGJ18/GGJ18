﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using UnityCommon;
using UnityEngine;
using UnityEngine.UI;

public class Boat : MonoBehaviour {

    //public static Vector2 windDirection = new Vector2(0.75f, 0.1f).normalized;

	public GameManager gameManager;
    [Header("Handling")]
    public float maxSpeed = 5f;
    public float minSpeed = 1f;
    public float turnRate = 1f;
    public float accelerationSmooth = 1 / 4f;
    public float decelerationSmooth = 1 / 4f;

    [Header("View")]
    public float swayPeriodTime = 4f;
    public float maxSway = 10f;
    //public float windPeriodTime = 3f;
    //public float windSway = 1f;
    //public float windOffset = 0.4f;

    [Header("Debug Only")]
    public float velocity;
    public float swayOffset;

    private float time;

    public void BoatPhysics(float time) {
        var sway = maxSway * Mathf.Sin(((time / swayPeriodTime) + swayOffset) * 2 * Mathf.PI);
        transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, sway);

        //var windSwayAtT = windSway * Mathf.Sin(((time / windPeriodTime) + windOffset) * 2 * Mathf.PI);
        //var windRotation = Quaternion.AngleAxis(windSwayAtT, Axes.FromUpForward(Vector3.up, windDirection.PromoteXZ(0)).right);
        //transform.rotation = transform.rotation * windRotation;
    }

    // Use this for initialization
    void Start() {
        velocity = minSpeed;
        swayOffset = UnityEngine.Random.value;
    }

    public void HandleMovement(float h, float v) {
        transform.Rotate(0, h * turnRate * Time.deltaTime, 0);

        if( v > 0f )
            velocity = Mathf.Lerp(velocity, maxSpeed, accelerationSmooth * Time.deltaTime);
        else if( v < 0f )
            velocity = Mathf.Lerp(velocity, minSpeed, decelerationSmooth * Time.deltaTime);

        var forwardOnPlane = new Plane(Vector3.up, Vector3.zero).ClosestPointOnPlane(transform.forward).normalized;
        transform.position += velocity * Time.deltaTime * forwardOnPlane;
    }

    // Update is called once per frame
    void Update() {
        time += Time.deltaTime;
        BoatPhysics(time);
    }

	void OnTriggerEnter(Collider col)
	{
        if( col.gameObject.layer == LayerMask.NameToLayer("Baddies") ) {
            gameManager.LoseGameFromCollisionWithBaddie(col.transform);
        }
	}
}

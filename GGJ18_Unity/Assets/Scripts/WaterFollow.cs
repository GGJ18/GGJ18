﻿// created by Alex

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFollow : MonoBehaviour {

	public Boat boat;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		transform.position = new Vector3 (boat.transform.position.x, transform.position.y, boat.transform.position.z);
		
	}
}

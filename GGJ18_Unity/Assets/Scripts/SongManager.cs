﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Common;
using UnityCommon;
using TMPro;

public class SongManager : MonoBehaviour {

    [Serializable]
    public struct SongInfo {
        public string name;
		public Color color;
        public string band;
        public int popularity;
        public int velocity;
        public List<bool> recentPlays;
        public int numRecentPlays { get { return recentPlays.Count((x) => x); } }
        public int minPlaysTillOverplayLoss;
        public int maxPlaysTillUnderplayGain;
        public int overplayLossLength;
        public bool isOverplayed { get { return overplayLossLength > 0; } }
        public int underplayedGainLength;
        public bool isUnderplayed { get { return underplayedGainLength > 0; } }
        public string playStatusToString() {
            if( isOverplayed )
                return "overplayed";
            else if( isUnderplayed )
                return "underplayed";
            else
                return "regular";
        }

        public override string ToString() {
            return string.Format("{0} [{1}] " +
                "pop: {2}, vel: {3}, #plays: {4} " +
                "{5}: -{6}, +{7} " +
                "(-{8}, +{9})",
                name, band,
                popularity, velocity, numRecentPlays,
                playStatusToString(), overplayLossLength, underplayedGainLength,
                minPlaysTillOverplayLoss, maxPlaysTillUnderplayGain);
        }
    }

    [Serializable]
    public struct PlayInfo {
        public int score;
        public float duration;

        public PlayInfo(int _score, float _duration) {
            score = _score;
            duration = _duration;
        }
    }

    public GameManager gameManager;

    public SongNameInfo songNameInfo;

    //public int targetListeners;
    public int numSongs = 10;
    public int maxSongsInPlaylist = 5;

    [Header("Popularity")]
    public int minStartPopularity = 0;
    public int maxStartPopularity = 5;
    public int numPlaysToTrack = 15;
    public int noPlaysTillGainAverage = 5;
    public int noPlaysTillGainDeviation = 3;
    public int playsTillLossAverage = 9;
    public int playsTillLossDeviation = 4;
    public int numPlaysOverplayedLength = 20;
    public int numPlaysUnderplayedLength = 15;
    public int underplayGainVelocity = 1;
    public int overplayLossVelocity = -1;

    [Header("Listeners")]
    public int numStartListeners = 2000;
    public AnimationCurve listenersPerPlayByRanking;

    [Header("Radio Play")]
    public float scoreTime = 30;
    public float songPlayTime = 5f;

    [Header("View")]
    UnityEngine.UI.Image playTimeSlider;
	public GameObject recordBufferPanel;
	public GameObject recordBufferPrefab;
    public TextMeshProUGUI listenerScoreText;
    public PlaylistView playlistView;
    public AudioSource radioMusic;
    public AudioSource radioStatic;

    [Range(0f, 1f)]
    public float songColorSaturationMin = 1f;
    [Range(0f, 1f)]
    public float songColorSaturationMax = 1f;
    [Range(0f, 1f)]
    public float songColorVibrancy = 0.5f;

    [Header("Debug Only")]
    public int listeners;
    public List<PlayInfo> recentRadioPlay;
    public List<SongInfo> songs;
    public List<int> availableSongs { get { return songs.Select((song, idx) => idx).Where((songIdx) => !playlist.Contains(songIdx)).ToList(); } }
    public List<int> rankings;
    public List<int> playlist;
    public List<int> songNotepad;

    private StateMachine pStateMachine = new StateMachine();
    private UpdateTimer pSongPlaytime;

	public Boat playerBoat;

	float sceneTime;

    public SongInfo CreateSong() {
        var newSong = new SongInfo();
        newSong.name = SongNameInfo.GenerateSongName(songNameInfo);
        newSong.band = Custom.Random.Select(songNameInfo.bandNames);
		newSong.color = Color.HSVToRGB(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(songColorSaturationMin, songColorSaturationMax), songColorVibrancy);
        newSong.popularity = UnityEngine.Random.Range(minStartPopularity, maxStartPopularity + 1);
        newSong.velocity = Custom.Random.Select(new List<int> { overplayLossVelocity, 0, underplayGainVelocity });
        newSong.recentPlays = new List<bool>();
        newSong.minPlaysTillOverplayLoss = (int) Custom.Random.BoundedNormalDist(playsTillLossAverage - playsTillLossDeviation, playsTillLossAverage + playsTillLossDeviation);
        newSong.maxPlaysTillUnderplayGain = (int) Custom.Random.BoundedNormalDist(noPlaysTillGainAverage - noPlaysTillGainDeviation, noPlaysTillGainAverage + noPlaysTillGainDeviation);
        newSong.overplayLossLength = 0;
        newSong.underplayedGainLength = 0;
        return newSong;
    }

    public SongInfo PrestartAdvance(SongInfo song, bool played) {
        song.recentPlays.Add(played);

        song.overplayLossLength = Mathf.Max(0, song.overplayLossLength - 1);
        song.underplayedGainLength = Mathf.Max(0, song.underplayedGainLength - 1);

        if( song.numRecentPlays >= song.minPlaysTillOverplayLoss ) {
            song.overplayLossLength = numPlaysOverplayedLength;
            song.underplayedGainLength = 0;
        }
        else if( song.numRecentPlays <= song.maxPlaysTillUnderplayGain ) {
            song.underplayedGainLength = numPlaysUnderplayedLength;
            song.overplayLossLength = 0;
        }

        return song;
    }

    public SongInfo AdvanceSong(SongInfo song, bool played) {
        if( song.recentPlays.Count() > numPlaysToTrack )
            song.recentPlays.RemoveAt(0);

        song = PrestartAdvance(song, played);

        if( song.isOverplayed )
            song.velocity = overplayLossVelocity;
        else if( song.isUnderplayed )
            song.velocity = underplayGainVelocity;
        else
            song.velocity = 0;

        song.popularity += song.velocity;

        return song;
    }

    // todo: what if songs have same popularity?
    public void DetermineSongRankings() {
        rankings = songs.Select((song, idx) => Pair.Make(idx, song)).OrderByDescending((pair) => pair.second.popularity).Select((pair) => pair.first).ToList();
    }

    public List<SongInfo> SongsByRanking() {
        return rankings.Select((idx) => songs[idx]).ToList();
    }

    public int ListenerScoreForSongPlay(int songIdx) {
        var rankIdx = rankings.IndexOf(songIdx);
        return (int) listenersPerPlayByRanking.Evaluate(rankIdx);
    }

    public void InitializeSongList() {
        songs = new List<SongInfo>(numSongs);
        for( int i = 0; i < numSongs; ++i )
            songs.Add(CreateSong());

        for( int i = 0; i < 15; ++i ) {
            var pseudoPlaylist = Custom.Random.Select(songs.Select((song, idx) => idx), maxSongsInPlaylist);
            songs = songs.Select((song, idx) => PrestartAdvance(song, pseudoPlaylist.Contains(idx))).ToList();
        }
        playlist = new List<int>(maxSongsInPlaylist);
    }

    private void UpdateSongInfo(int songIdx) {
        songs = songs.Select((song, idx) => AdvanceSong(song, songIdx == idx)).ToList();
    }

    // Use this for initialization
    void Start() {
        InitializeSongList();
		sceneTime = Time.time;

		playerBoat = GameObject.Find ("ship").GetComponent<Boat>();

//        Debug.Log("Songs:");
//        songs.ForEach((song) => Debug.Log(song));

        recentRadioPlay = new List<PlayInfo> { new PlayInfo(0, scoreTime) };
        radioStatic.Play();

        //for(int i = 0; i < 15; ++i ) {
        //    playlist = Custom.Random.Select(songs.Select((song, idx) => idx), maxSongsInPlaylist);
        //    songs = songs.Select((song, idx) => AdvanceSong(song, playlist.Contains(idx))).ToList();
        //    DetermineSongRankings();
        //    var listenersThisTurn = playlist.Sum((idx) => ListenerScoreForSongPlay(idx));
        //    listeners += listenersThisTurn;
        //    Debug.Log("Songs:");
        //    SongsByRanking().ForEach((song) => Debug.Log(song));
        //    Debug.LogFormat("listeners: {0}, this turn: {1}", listeners, listenersThisTurn);
        //}
    }

    private void PlayNextSong() {
        if( !radioMusic.isPlaying ) {
            radioMusic.Play();
            radioStatic.Stop();
            gameManager.hasPromptBeenFulfilled = true;
            gameManager.DeactivatePrompt();
        }

        var playInfo = new PlayInfo(ListenerScoreForSongPlay(playlist[0]), 0f);
        recentRadioPlay.Add(playInfo);

        pStateMachine.PreserveState(ref pSongPlaytime, 1 / 60f, songPlayTime, (t) => {
			recordBufferPanel.transform.GetChild(0).GetComponent<Image>().fillAmount = 1-t;
        }, () => {
            UpdateSongInfo(playlist[0]);
            DetermineSongRankings();

			Destroy(recordBufferPanel.transform.GetChild(0).gameObject);
            playlist.RemoveAt(0);
            playlistView.RemovePlayingSong();
			playTimeSlider = null;

            if( playlist.Any() ) {
                PlayNextSong();
            }
            else {
                radioMusic.Stop();
                radioStatic.Play();
                recentRadioPlay.Add(new PlayInfo(0, 0f));
            }
        });
    }

    public void AddSongToPlaylist(int songIdx) {
        playlist.Add(songIdx);
		var recordBuffer = Instantiate (recordBufferPrefab,recordBufferPanel.transform,false);
        if(playlist.Count() == 1)
            PlayNextSong();
    }

    private int ListenerScore() {
        float score = numStartListeners;

        float totalTime = 0f;
        for( int i = recentRadioPlay.Count() - 1; i >= 0; --i ) {
            if( totalTime <= scoreTime ) {
                if( totalTime + recentRadioPlay[i].duration <= scoreTime )
                    score += recentRadioPlay[i].duration * recentRadioPlay[i].score;
                else
                    score += (scoreTime - totalTime) * recentRadioPlay[i].score;
                totalTime += recentRadioPlay[i].duration;
            }
            else {
                recentRadioPlay.RemoveAt(i);
            }
        }

        return (int) score;
    }

    // Update is called once per frame
    void Update() {
		//if(playTimeSlider != null)
        pStateMachine.Update();

        if( recentRadioPlay.Any()) {
            var currentPlayInfo = recentRadioPlay.Last();
            currentPlayInfo.duration += Time.deltaTime;
            recentRadioPlay[recentRadioPlay.Count() - 1] = currentPlayInfo;
        }
        else {
            recentRadioPlay.Add(new PlayInfo(0, Time.deltaTime));
        }

        listenerScoreText.text = string.Format("Listeners: {0}", ListenerScore());

        //if( Time.time - sceneTime > 10 && ListenerScore() <= numStartListeners ) {
        //    gameManager.LoseGameFromMinimumListeners()();
        //}
    }
}

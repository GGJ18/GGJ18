﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common;
using UnityCommon;

[CreateAssetMenu(fileName = "SongNameInfo", menuName = "Scriptable Object/SongNameInfo")]
public class SongNameInfo : ScriptableObject {

    public enum WordType {
        Noun,
        Verb,
        Adjective,
    }

    public class SongNameToken { }
    public class NounToken : SongNameToken { };
    public class VerbToken : SongNameToken { };
    public class AdjToken : SongNameToken { };
    public class StringToken : SongNameToken { public string text; };
    public struct SongName { public List<SongNameToken> list; }

    public List<string> nouns;
    public List<string> verbs;
    public List<string> adjectives;

    public List<string> songNameTemplates;

    public List<string> bandNames;

    public static string GenerateSongName(SongNameInfo songNameInfo) {
        var songName = Custom.Random.Select(songNameInfo.songNameTemplates);

        Action<string, List<string>> replaceTokenWithInstance = (token, tokenList) => {
            var tokenIdx = 0;
            while( 0 <= tokenIdx && tokenIdx < songName.Count() ) {
                tokenIdx = songName.IndexOf(token, tokenIdx);
                if( tokenIdx >= 0 ) {
                    songName = songName.Remove(tokenIdx, token.Count());
                    var word = Custom.Random.Select(tokenList);
                    if( tokenIdx < songName.Count() )
                        songName = songName.Insert(tokenIdx, word);
                    else
                        songName += word;
                    tokenIdx += word.Count();
                }
            }
        };
        replaceTokenWithInstance("<noun>", songNameInfo.nouns);
        replaceTokenWithInstance("<verb>", songNameInfo.verbs);
        replaceTokenWithInstance("<adjective>", songNameInfo.adjectives);
        return songName;
    }
}

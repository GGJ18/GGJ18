﻿// created by Alex

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleSceneBehaviour : MonoBehaviour {

	[SerializeField] Button newGameButton;
	[SerializeField] Button quitGameButton;

	// Use this for initialization
	void Start () {

		quitGameButton.onClick.AddListener(() => {Application.Quit();});
		newGameButton.onClick.AddListener(() => {SceneManager.LoadScene(1);});
		
	}
}

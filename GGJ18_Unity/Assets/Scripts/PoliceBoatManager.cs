﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using UnityCommon;
using UnityEngine;
using URand = UnityEngine.Random;

public class PoliceBoatManager : MonoBehaviour {

    public GameObject playerShip;
    public GameObject policeParent;
    public GameObject policeBoatPrefab;
    public int maxPoliceSimultaneous = 3;
    public float chancePerCheck = 0.1f;
    public float timePerCheck = 5f;
    public float spawnRadius = 100f;
    public float despawnRadius = 100f;

    private StateMachine pStateMachine = new StateMachine();

    [Header("Debug Only")]
    public List<PoliceBoat> policeBoats;

    private void CheckSpawn() {
        if( policeBoats.Count() < maxPoliceSimultaneous && URand.value < chancePerCheck ) {
            Vector2 randDirection;
            do {
                randDirection = URand.insideUnitCircle.normalized;
            } while( Vector3.Angle(playerShip.transform.forward, randDirection.PromoteXZ(0)) > 90f );

            var go = Instantiate(policeBoatPrefab,
                playerShip.transform.position + (spawnRadius * randDirection).PromoteXZ(0),
                Quaternion.Euler(0, URand.Range(0, 360f), 0),
                policeParent.transform);
            var policeComp = go.GetComponent<PoliceBoat>();
            policeBoats.Add(policeComp);
        }
    }

    // Use this for initialization
    void Start() {
        QueueSpawn();
    }

    void QueueSpawn() {
        pStateMachine.AddDelay(timePerCheck, () => { CheckSpawn(); QueueSpawn(); });
    }

    // Update is called once per frame
    void Update() {
        pStateMachine.Update();

        for(int i = 0; i < policeBoats.Count(); ) {
            if((policeBoats[i].transform.position - playerShip.transform.position).magnitude > despawnRadius) {
                Destroy(policeBoats[i].gameObject);
                policeBoats.RemoveAt(i);
            }
            else {
                ++i;
            }
        }
    }
}

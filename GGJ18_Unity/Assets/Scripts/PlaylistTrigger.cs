﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Common;
using UnityCommon;

public class PlaylistTrigger : MonoBehaviour {
    public PlaylistView playlistView;

    public float triggerTime = 1f;
    public float leaveTriggerTime = 0.5f;

    private StateMachine pStateMachine = new StateMachine();
    private SimpleTimer pDelayTimer;
    private SimpleTimer pLeaveDelayTimer;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        pStateMachine.Update();
    }

    private void OnTriggerEnter(Collider other) {
        if( other.gameObject.layer == LayerMask.NameToLayer("Player") ) {
            pStateMachine.PreserveDelay(ref pDelayTimer, triggerTime, () => {
                pStateMachine.Remove(ref pLeaveDelayTimer);
                playlistView.Init();
            });
        }
    }

    private void OnTriggerExit(Collider other) {
        if( other.gameObject.layer == LayerMask.NameToLayer("Player") ) {
            pStateMachine.RestartDelay(ref pLeaveDelayTimer, leaveTriggerTime, () => {
                pStateMachine.Remove(ref pDelayTimer);
                playlistView.Close();
            });
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using UnityCommon;
using UnityEngine;

public class PoliceBoat : MonoBehaviour {

    private Boat boat;

    private void Awake() {
        this.TieComponents(ref boat);
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        boat.HandleMovement(0, 0);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Common;
using UnityCommon;

public class BoatControlsTrigger : MonoBehaviour {
    public GameManager gameManager;

    public bool isLeft;
    public float triggerTime = 1f;

    private StateMachine pStateMachine = new StateMachine();
    private SimpleTimer pDelayTimer;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        pStateMachine.Update();
    }

    private void OnTriggerStay(Collider other) {
        if( other.gameObject.layer == LayerMask.NameToLayer("Player") )
            pStateMachine.PreserveDelay(ref pDelayTimer, triggerTime, () => gameManager.SwitchToThirdPerson(isLeft));
    }

    private void OnTriggerExit(Collider other) {
        if( other.gameObject.layer == LayerMask.NameToLayer("Player") )
            pStateMachine.Remove(ref pDelayTimer);
    }
}

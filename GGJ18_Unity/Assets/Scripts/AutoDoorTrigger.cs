﻿// created by Alex

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

public class AutoDoorTrigger : MonoBehaviour {

    public GameManager gameManager;
	[SerializeField] Transform door;
	[SerializeField] GameObject insideGraphics;

	bool doorOpen;

	// Use this for initialization
	void Start () {
		doorOpen = false;
        insideGraphics.SetActive(doorOpen);
    }
	
	// Update is called once per frame
	void Update () {

		if (doorOpen) 
		{
			door.transform.localRotation = Quaternion.Lerp (door.transform.localRotation, Quaternion.Euler (new Vector3 (-90, 0, 179f)), 0.05f);
		}
		else 
		{
			door.transform.localRotation = Quaternion.Lerp (door.transform.localRotation, Quaternion.Euler (new Vector3 (-90, 0, 0f)), 0.05f);
		}
		
	}

	void OnTriggerEnter(Collider other) {
        if(other.gameObject.layer == LayerMask.NameToLayer("Player"))
		    doorOpen = true;

		StopCoroutine ("HideInside");
		insideGraphics.SetActive (true);
        gameManager.ActivatePrompt();
    }

	void OnTriggerExit(Collider other) {
        if( other.gameObject.layer == LayerMask.NameToLayer("Player") )
            doorOpen = false;

		StartCoroutine ("HideInside");
        gameManager.DeactivatePrompt();

    }

	IEnumerator HideInside()
	{
		yield return new WaitForSeconds (2f);
		insideGraphics.SetActive (false);

	}
}

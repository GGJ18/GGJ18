﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using UnityCommon;
using UnityEngine;
using TMPro;

public class PlaylistView : MonoBehaviour {

    public GameManager gameManager;
    public List<GameObject> playlistGameObjects;
    public Camera mainCamera;
    public SongManager songManager;
    public Canvas canvas;
    public RectTransform playlist;
    public RectTransform availableSongs;
    public GameObject songTextPrefab;
    public float songTextHeight = 100f;

    [Header("Debug Only")]
    public bool isActive = false;
    public List<Pair<RectTransform, TextMeshProUGUI>> availableTexts = new List<Pair<RectTransform, TextMeshProUGUI>>();
    public List<Pair<RectTransform, TextMeshProUGUI>> playlistTexts = new List<Pair<RectTransform, TextMeshProUGUI>>();

    private Camera pCamera;

    private int selectedAvailableSong = -1;
    private Vector2? lastMousePosition;

    public int localToIdx(float y) {
        return (int) (-y / songTextHeight);
    }

    public float idxToLocal(int idx) {
        return -idx * songTextHeight;
    }

	public Pair<RectTransform, TextMeshProUGUI> InstantiateSongText(int listIdx, int songIdx, SongManager.SongInfo song, Transform parent, Color col) {
        var textGo = Instantiate(songTextPrefab, parent);
		var text = textGo.GetComponentInChildren<TextMeshProUGUI>();
        var rectT = textGo.GetComponent<RectTransform>();
        text.text = string.Format("{0} \n- {1}", song.name, song.band);
		text.color = col;
        rectT.anchoredPosition = new Vector2(0, idxToLocal(listIdx));
        return Pair.Make(rectT, text);
    }

    private void InitPlaylist() {
        songManager.playlist.ForIndexEach((playlistIdx, songIdx) => {
            var song = songManager.songs[songIdx];
			var text = InstantiateSongText(playlistIdx, songIdx, song, playlist.transform, songManager.songs[songIdx].color);
            playlistTexts.Add(text);
        });
    }

    private void InitAvailableSongs() {
        songManager.availableSongs.ForIndexEach((availableIdx, songIdx) => {
            var song = songManager.songs[songIdx];
			var text = InstantiateSongText(availableIdx, songIdx, song, availableSongs.transform,songManager.songs[songIdx].color);
            availableTexts.Add(text);
        });
    }

    public void RemovePlayingSong() {
        if(isActive) {
            Destroy(playlistTexts[0].first.gameObject);
            playlistTexts.RemoveAt(0);
            for( int playlistIdx = 0; playlistIdx < playlistTexts.Count(); ++playlistIdx ) {
                var playlistText = playlistTexts[playlistIdx];
                playlistText.first.anchoredPosition = new Vector2(0, idxToLocal(playlistIdx));
            }
            ClearAvailableSongs();
            InitAvailableSongs();
        }
    }

    public void ClearPlaylist() {
        playlistTexts.ForEach((comp) => Destroy(comp.first.gameObject));
        playlistTexts.Clear();
    }

    public void ClearAvailableSongs() {
        availableTexts.ForEach((comp) => Destroy(comp.first.gameObject));
        availableTexts.Clear();
    }

    public void Init() {
        if(!isActive) {
            isActive = true;
            playlistGameObjects.ForEach((x) => x.SetActive(true));
            InitPlaylist();
            InitAvailableSongs();
            gameManager.DeactivatePrompt();
            gameManager.firstPersonController.isMouseControlEnabled = false;
            gameManager.firstPersonController.SetCursorLock(false);
        }
    }

    public void Close() {
        isActive = false;
        playlistGameObjects.ForEach((x) => x.SetActive(false));
        ClearPlaylist();
        ClearAvailableSongs();
        gameManager.ActivatePrompt();
        gameManager.firstPersonController.isMouseControlEnabled = true;
        gameManager.firstPersonController.SetCursorLock(true);
    }

    private void Awake() {
        // Get a reference to the camera if Canvas Render Mode is not ScreenSpace Overlay.
        if( canvas.renderMode == RenderMode.ScreenSpaceOverlay )
            pCamera = null;
        else
            pCamera = canvas.worldCamera;
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if( !isActive )
            return;

        if( Input.GetKeyDown(KeyCode.Escape) )
            Close();

        if( selectedAvailableSong < 0 && Input.GetMouseButtonDown(0)) {
			var availableIdx = availableTexts.FindIndex((candidate) => TMP_TextUtilities.IsIntersectingRectTransform(candidate.first, Input.mousePosition, pCamera));
            if( availableIdx >= 0 ) {
				// ?? is rectT doing anything here?
                var rectT = availableTexts[availableIdx].first;
                selectedAvailableSong = availableIdx;
				lastMousePosition = Input.mousePosition;
            }
        }
        else if( selectedAvailableSong >= 0 ) {
            var songComp = availableTexts[selectedAvailableSong];
			var rectT = songComp.first;
            if( Input.GetMouseButton(0) ) {
				rectT.anchoredPosition = rectT.anchoredPosition + ((Vector2) Input.mousePosition - lastMousePosition.Value) / canvas.scaleFactor;
				lastMousePosition = Input.mousePosition;
            }
            else {
                var worldPt = rectT.TransformPoint(rectT.rect.center).WithZ(mainCamera.farClipPlane);
                var playlistPt = playlist.InverseTransformPoint(worldPt);
                if( playlist.rect.Contains(playlistPt) && songManager.playlist.Count() < songManager.maxSongsInPlaylist ) {
                    var songIdx = songManager.availableSongs[selectedAvailableSong];
                    availableTexts.RemoveAt(selectedAvailableSong);
                    songManager.AddSongToPlaylist(songIdx);
                    playlistTexts.Add(songComp);
                    songComp.first.SetParent(playlist.transform);
					rectT.anchoredPosition = new Vector2(0, idxToLocal(playlistTexts.Count() - 1));

                    for(int availableIdx = selectedAvailableSong; availableIdx < availableTexts.Count(); ++availableIdx ) {
                        var availableText = availableTexts[availableIdx];
						availableText.first.anchoredPosition = new Vector2(0, idxToLocal(availableIdx));
                    }

                    //var playlistIdx = localToIdx(playlistPt.y);
                    //if(playlistIdx >= songManager.playlist.Count()) {
                    //    playlistIdx = songManager.playlist.Count();
                    //    songManager.playlist.Add(selectedAvailableSong);
                    //    playlistTexts.Add(songComp);
                    //}
                    //else {
                    //    songManager.playlist.Insert(playlistIdx, selectedAvailableSong);
                    //    playlistTexts.Insert(playlistIdx, songComp);
                    //}

                    //songTexts.RemoveAt(selectedAvailableSong);

                    //songComp.transform.SetParent(playlist.transform);
                    //songRect.anchoredPosition = new Vector2(0, idxToLocal(playlistIdx));
                }
                else {
					rectT.anchoredPosition = new Vector2(0, -selectedAvailableSong * songTextHeight);
                }
                selectedAvailableSong = -1;
            }
        }
    }
}

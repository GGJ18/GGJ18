﻿// created by Alex
// some parts modified by Jonathan

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Common;
using UnityCommon;

public class RadarScript : MonoBehaviour {

	public Image radarBG;
	[SerializeField] GameObject radarBlipPrefab;
    [SerializeField] GameObject boatDirectionPrefab;

    public Transform boatDirectionParent;
    public PoliceBoatManager policeBoatManager;
	public IcebergManager icebergManager;
	public Boat boat;
    public FPController firstPersonController;
    public AudioSource radarPingAudio;

    public GameObject boatDirection;

    public float maxRadarRange = 100f;

	// Use this for initialization
	void Start () {
        boatDirection = Instantiate(boatDirectionPrefab, boatDirectionParent, false);
	}
	
	// Update is called once per frame
	void Update () {
		radarBG.rectTransform.Rotate (0, 0, 90*Time.deltaTime);

		int numBlipsReqd = policeBoatManager.policeBoats.Count + icebergManager.items.Count;
		int childCount = transform.childCount;

		if (childCount < numBlipsReqd) 
		{
			StartCoroutine (FlashRoutine());

			for (int i = numBlipsReqd - childCount; i > 0; i--) 
			{
				InstantiateBlip ();
			}
		}

		if (childCount > numBlipsReqd) 
		{
			for (int i = childCount - numBlipsReqd; i > 0; i--) 
			{
				DestroyBlip ();
			}
		}

		int blipIndex = 0;

		for (int i = 0; i < policeBoatManager.policeBoats.Count; i++) 
		{
			var blip = transform.GetChild (blipIndex);

			Vector3 blipVector = boat.transform.InverseTransformPoint (policeBoatManager.policeBoats[i].transform.position);
            if( firstPersonController.gameObject.activeInHierarchy ) {
                var fpAngle = VectorExt.SAngleAroundAxis(boat.transform.forward,
                    firstPersonController.gameObject.transform.forward, Vector3.up);
                blipVector = Quaternion.Euler(0, -fpAngle, 0) * blipVector;
            }
			blipVector = Vector3.ClampMagnitude (blipVector, maxRadarRange);

			blip.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (blipVector.x, blipVector.z);

			blipIndex++;
		}

		for (int i = 0; i < icebergManager.items.Count; i++) 
		{
			var blip = transform.GetChild (blipIndex);

			Vector3 blipVector = boat.transform.InverseTransformPoint (icebergManager.items[i].transform.position);
            if( firstPersonController.gameObject.activeInHierarchy ) {
                var fpAngle = VectorExt.SAngleAroundAxis(boat.transform.forward,
                    firstPersonController.gameObject.transform.forward, Vector3.up);
                blipVector = Quaternion.Euler(0, -fpAngle, 0) * blipVector;
            }
            blipVector = Vector3.ClampMagnitude (blipVector, maxRadarRange);

			blip.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (blipVector.x, blipVector.z);

			blipIndex++;
		}

        // boat direction:
        {
            Vector3 directionVector = Vector3.forward;
            Vector2 boatDirection2d = new Vector2(directionVector.x, directionVector.z);
            if( firstPersonController.gameObject.activeInHierarchy ) {
                var fpAngle = VectorExt.SAngleAroundAxis(boat.transform.forward,
                    firstPersonController.gameObject.transform.forward, Vector3.up);
                directionVector = Quaternion.Euler(0, -fpAngle, 0) * directionVector;
            }
            directionVector = directionVector.normalized * maxRadarRange;

            var directionVector2d = new Vector2(directionVector.x, directionVector.z);
            boatDirection.GetComponent<RectTransform>().anchoredPosition = directionVector2d;
            boatDirection.GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, Vector2.SignedAngle(boatDirection2d, directionVector2d));
        }
    }

	IEnumerator FlashRoutine()
	{
		radarBG.color = Color.red;
		Color tmpCol = radarBG.color;

		while (radarBG.color.g < 1) 
		{
			tmpCol.g += Time.deltaTime;
			tmpCol.b += Time.deltaTime;
			radarBG.color = tmpCol;
			yield return null;
		}
	}

	void InstantiateBlip()
	{
        radarPingAudio.Play();
		var blip = Instantiate (radarBlipPrefab,transform,false);
		blip.transform.localScale = new Vector3 (0.2f, 0.2f, 0.2f);
	}

	void DestroyBlip()
	{
		GameObject.Destroy(transform.GetChild (0).gameObject);
	}

}
